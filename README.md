# todo

***A new*** Flutter project.:

## Getting Started

This project is a starting point for a Flutter application.

### To run this project

- **ensure you have flutter sdk and necessary dependencies**
- **Clone the repository and navigate to the folder**
- **Run** ```flutter pub get```
- **Run** ```flutter run -d chrome```

#### To see the Preview
[todoweb](https://mohammed-fawaz-cp.github.io/todoweb/)


#### To Download the build File
[todowebBuildFile](https://github.com/mohammed-fawaz-cp/todoweb.git) Clone this file Run it any local sever

