import 'package:flutter/material.dart';


class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final padding = width * 0.05;

    return Scaffold(
      body:  Container(
        width: width,height: height,
        decoration: BoxDecoration(
          //gradient: LinearGradient(colors: [primaryColor, primaryColor1],
        //  ),
        ),
        child: Center(
          child: Container(width: 300,height: 300,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(22),
              //color: secondaryColor1.withOpacity(0.7)
              ),
              child: Padding(
                padding: const EdgeInsets.all(11),
                child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  SizedBox(height: 10,),
                  Text('Login',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25,//color:// primaryColor
                  ),),
                  SizedBox(height: 10,),
                  TextFormField(
                    decoration: InputDecoration(enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(//color: //primaryColor
                        ),
                        
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(//color: primaryColor
                        ),
                        
                      ),
                      labelText: 'Email',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(//color: primaryColor
                        ),
                        
                       
                      )
                      
                    ),
                  ),
                  SizedBox(height: 10,),
                  TextFormField(
                    decoration: InputDecoration(enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(//color: primaryColor
                        ),
                        
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(//color: primaryColor
                        ),
                        
                      ),
                      labelText: 'Password',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(//color: primaryColor
                        ),
                        
                       
                      )
                      
                    ),
                  ),
                  SizedBox(height: 10,),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                        child: Text('Login'),
                        style: ElevatedButton.styleFrom(
                         // backgroundColor: primaryColor,
                          //foregroundColor: secondaryColor1,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/todos');
                        },
                      ),
                      SizedBox(width: 10,),
                      //forget password
                      Text('Forget Password! ',style: TextStyle(
                        textBaseline: TextBaseline.alphabetic,
                      ),),
                    ],
                  )
                ],),
              ),
            ),
        ),
        ),
      
    ) ;
    }
}
