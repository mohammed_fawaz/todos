import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo/Apiserverlink/supabase.dart';
import 'package:todo/screens/markdown.dart';
import 'package:todo/variables/statecontroll.dart';

class TodoBody extends ConsumerWidget {
   TodoBody({super.key});
  final todostextController = TextEditingController();
  final descriptiontextController = TextEditingController();
  int completedtodo = 0;
  @override
  Widget build(BuildContext context,WidgetRef ref) {
    
    final watchProjectIndex = ref.watch(projectStateindex) as int;
    final listproject = ref.watch(ProjectDataFetchProvider);
   // final completedtask = ref.watch(numberofCompletedProject(watchProjectIndex));
    //print("${completedtask}");
    
   // print("${completedtask.value}");
    return listproject.when(
      data: (data) {
        final completedtask = ref.watch(funcCompProvider(data[watchProjectIndex]['idpr'])).value;
        if(watchProjectIndex.isNegative|| watchProjectIndex.isNaN){
          //  ref.watch(projectStateindex.notifier).update((state) => {
          //   'index':0,
          //     'idpr':data[0]['idpr']

          //  });
          
            
            Navigator.pushReplacementNamed(context, '/todos');
          
        }
        if (data.isEmpty) {
          return Container(child: Center(child: Text('Create a Project',style: TextStyle(
            fontSize: 17
          ),)));
        } 
        else {
          //handle range error
          
          final todolist = data[watchProjectIndex]['todos'];
          final todolistlenght = todolist.length;
          final todoprojecttitle = data[watchProjectIndex]['title'];
          
          
          return Container(
      child: Column(
        children: [
          Expanded(flex: 2, child:
          ListTile(leading: IconButton(onPressed: () {
          //  print('PRINT DATA TYPE ::${data[watchProjectIndex]}');
            Navigator.pushNamed(context, '/myp',arguments: {
              "data":data[watchProjectIndex],
              "completedTodo":completedtask,
              "totalTodo":todolistlenght
            },);
          }, icon: Icon(Icons.edit_document),style: ButtonStyle(
            iconSize: MaterialStateProperty.all<double>(11),
          ),),
            title:Text("Project Title : ${todoprojecttitle}") ,
          subtitle: Text('${data[watchProjectIndex]['createdDate']}'),
          trailing: SizedBox(width: 80,height: 40,
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Column(children: [
                Text('Todos'),
                Text('${todolistlenght}')
              ],),Column(children: [
                Text('done'),
                Text('${completedtask}')
              ],)
            ],),
          ),) ),
          Expanded(flex: 8, child: ListView.builder(itemBuilder: (context, index) {
            final previousStatusValue = todolist[index]['status'];
            return Card(
              color:todolist[index]['status']==true?Color.fromARGB(197, 181, 212, 192):null ,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
              child: ListTile(dense: true,
              trailing: IconButton(onPressed: ()async {
                ref.read(deleteTodoProvider(todolist[index]['id'].toString()));
                await ref.refresh(ProjectDataFetchProvider.future);
                ref.refresh(funcCompProvider(data[watchProjectIndex]['idpr']).future);
              }, icon: Icon(Icons.delete,size: 13,)),
              leading: Checkbox(shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6)),
                value: todolist[index]['status'],
                 onChanged: (value) async{
                 late final updatedDate;
                  if(previousStatusValue==false && value==true ){
                    updatedDate = DateTime.now().toString().split(' ')[0];
                  }else{
                    updatedDate = null;
                  }
               // print(value);
                UpdateDataTodo(value!, todolist[index]['id'],updatedDate);
                await ref.refresh(ProjectDataFetchProvider.future);
                  ref.refresh(funcCompProvider(data[watchProjectIndex]['idpr']).future);
              },),
              
                title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(todolist[index]['todos'].toString()),
                    Row(
                      children: [
                      Text('Created Date : ${todolist[index]['createdDate']}',
                      ),
                      SizedBox(width: 20,),
                      //Text(todolist[index]['updatedDate']!=null? 'Updated Date : \n${todolist[index]['updatedDate']}':'Updated Date : \n'),
                    ],)
                  ],
                ),
                subtitle: Text(todolist[index]['description']!=null?todolist[index]['description'].toString():""),
              )
            );
          },itemCount: todolistlenght,
          )),
          SizedBox(height: 150,
            child:ListTile(
              minVerticalPadding: 3,style: ListTileStyle.list,
              title: SizedBox(height: 40,
                child: TextField(controller: todostextController,
                  smartDashesType: SmartDashesType.enabled,
                  decoration: textfieldDecoration(hintText: 'Title'),
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 20,bottom: 0),
                child: TextFormField(controller: descriptiontextController,
                  maxLines: 3,minLines: 3,
                  decoration: 
                textfieldDecoration(hintText: 'Description'),),
              ),trailing: Column(mainAxisAlignment: MainAxisAlignment.end,
                children: [SizedBox(),
                  IconButton(onPressed: ()async {

                    if (todostextController.text.isNotEmpty){
                      //print('${data[watchProjectIndex]["idpr"]}');
                        await TodoDataAdd(todostextController.text, 
                        DateTime.now().toString().split(" ")[0], 
                        descriptiontextController.text, data[watchProjectIndex]["idpr"], false);
                        todostextController.clear();
                        descriptiontextController.clear();
                        await ref.refresh(ProjectDataFetchProvider.future);
                    }
                  }, icon: Icon(Icons.send)),
                ],
              ),
            )),
        ],
      ),
    );
        }
      },
      loading: () => const Center(
        child: CircularProgressIndicator(),
      ),
      error: (error, stackTrace) => Text('$error'),
    );
  }
}



Widget randomContainer(){
  return Container(
    
    color: randomColor(),
  );
}
randomColor(){
  
  return Color((math.Random().nextDouble() * 0xFFAF).toInt()).withOpacity(1.0);
}

textfieldDecoration({String hintText = '',
bool isDense = false,bool sufix = false,
bool isCollapsed = false}){
  return InputDecoration(isDense: isDense,isCollapsed: isCollapsed,contentPadding: EdgeInsets.all(7),
  

    border: OutlineInputBorder(gapPadding: 10,
      borderRadius: BorderRadius.circular(10),
    ),
    hintText: hintText,
    hintStyle: TextStyle(
      fontSize: 18,
      color: Colors.grey,
    ),
  );
}