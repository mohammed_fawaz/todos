import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:todo/Apiserverlink/supabase.dart';
import 'package:todo/database/base.dart';

import 'package:todo/screens/todos.dart';
import 'package:todo/variables/statecontroll.dart';




class RegisterProject extends StatelessWidget {
   RegisterProject({super.key});
  
  final _projectTitleController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    
    //print('ddeded');
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(//backgroundColor: secondaryColor1,
      body: Container(
        child: Row(children: [
          Expanded(flex: 1,
            child: Card.filled(//color: primaryColor.withAlpha(200),
            elevation: 2,
              shape:
           ContinuousRectangleBorder(
            borderRadius: BorderRadius.all(Radius.zero)),
            child: SizedBox(height: height,
            child:Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(flex:1,child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Projects :',style: TextStyle(
                    fontSize: 20,
                   // color: secondaryColor1,
                  ),),
                )),
                Expanded(flex:8,child:
                 Container(child: listProject(),)),
                 
               Consumer(
                 builder: (context,ref,child) {
                  final _addProjectstate =  ref.watch(addProjectstate);
                   return AnimatedCrossFade(sizeCurve: Curves.easeInOut,
                   secondCurve: Curves.easeInOut,firstCurve: Curves.easeInOut,
                    crossFadeState: _addProjectstate?CrossFadeState.showSecond:CrossFadeState.showFirst,duration: Duration(milliseconds: 500),
                    firstChild:Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(alignment: Alignment.centerRight,
                        child: FloatingActionButton(mini: true,child: Icon(Icons.add),
                        heroTag: 'Add Project',tooltip: 'Add Project',
                          onPressed: () {
                            //!used set state
                            
                          ref.read(addProjectstate.notifier).state=true;
                        },),
                      ),
                    ), secondChild:Container(//color: secondaryColor1,
                    height: 50,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(controller: _projectTitleController,onSubmitted: (value)async {
                          await ProjectDataAdd(_projectTitleController.text,
                                           DateTime.now().toString().split(" ")[0]);
                                           ref.read(addProjectstate.notifier).state=false; 
                                        _projectTitleController.clear();
                                  Future.delayed(Duration(microseconds: 300),() {
                                    ref.refresh(ProjectDataFetchProvider.future);
                                  },);      
                        },
                                  decoration: InputDecoration(icon: FittedBox(
                                    child: IconButton(onPressed: () {
                                      //!used set state
                                        ref.read(addProjectstate.notifier).state=false;
                                        
                                      }, icon: Icon(Icons.close,color: Colors.red,)),
                                  ),
                                    contentPadding: EdgeInsets.all(12),
                                    border: OutlineInputBorder(),
                                    labelText: 'Project Title',
                                  )
                                ),
                      ),
                    ),
                   );
                 }
               )
              ],
            ) ,),)),
            Expanded(flex: 3,
              child:TodoBody())
        ],),
      ),
    );
  }
  // side bar
  Widget listProject(){
  return Consumer(
    builder: (context,ref,child) {
      final listproject = ref.watch(ProjectDataFetchProvider);
     return  listproject.when(data: (data) {
      
      int indexmax = data.length;
       return ListView.builder(shrinkWrap: true,
        itemBuilder: (context, index) {
          
        return Card(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
          child: ListTile(title: Text('${data[index]['title']}'),
          trailing: IconButton(onPressed: () async{
           
              
            
            ref.read(deleteprojectProvider(data[index]['idpr'].toString()));
            ref.refresh(ProjectDataFetchProvider.future);
            //if(data.isNotEmpty){
               // ref.invalidate(projectStateindex);
            await ref.read(projectStateindex.notifier).update((state) => state--);
            //}
          }, icon: Icon(Icons.delete,color: Colors.red,size: 15,),),
          onTap: ()async {
            //print('${data![index]['title']}');
            ref.refresh(ProjectDataFetchProvider.future);
            await ref.read(projectStateindex.notifier).update((state) => index);
            //Navigator.push(context, MaterialPageRoute(builder: (context) => ProjectPage(title: '${snapshot.data![index]['title']}',)));
          },),
        );
      
      }, 
       itemCount: indexmax);
     }, 
      error: (error, stackTrace) {
        return Text('Something went wrong');
      },
       loading: () {
         return Center(child: CircularProgressIndicator());
       },);
    }
  );
}

//todo delete fuction





}

