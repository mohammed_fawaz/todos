
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todo/screens/rich.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:html' as html;

class MarkDownPage extends StatelessWidget {
   MarkDownPage({super.key});
  
  ScrollController _controller =ScrollController(); 

  @override
  Widget build(BuildContext context) {
    final arg = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    final data = arg['data'];
    final completedTodo =arg['completedTodo']; 
    final totalTodo = arg['totalTodo'];
    //print("DATA DATA DATA:: ${arg}");
    final mrkData = markdownData(data,completedTodo,totalTodo);
    return Scaffold(appBar: AppBar(),
    body: Markdown(
                  data:mrkData ,
                ),floatingActionButton: FloatingActionButton(onPressed: () {
                  _saveAndDownloadMarkdown(data['title'],mrkData);
                },child: Icon(Icons.download),tooltip: 'Download',),
    );
  }

// Future<void> downloadFile(String url, String filename) async {
//   final anchor = html.AnchorElement(href: url)
//     ..download = filename
//     ..style.display = 'none';
//   html.document.body!.append(anchor);
//   anchor.click();
//   anchor.remove();
// }

}

markdownData(data,completedTodo,totalTodo) {
  final completed =formatedTrueRichText(data);
  final pending  = formatedFalseRichText(data);
  //print('s:: ;; ;;   $s');
  final markdownText = '''
# ${data['title']}

***Summary:*** ${completedTodo}/${totalTodo} todos completed

## Pending


${pending.map((task) => "- [ ] ${task['todos']}").join('\n')}


## Completed

${completed.map((task) => "- [x] ${task['todos']}").join('\n')}
''';
return markdownText;
}


 void _saveAndDownloadMarkdown(String fileName,data) {
    final String markdownContent = data;
    final html.Blob blob = html.Blob([markdownContent]);
    final html.AnchorElement anchorElement = html.AnchorElement(href: html.Url.createObjectUrlFromBlob(blob));
    anchorElement.download = '$fileName.md';
    anchorElement.click();
  }

formatedTrueRichText(data) {
  //print("DATA DATA DATA:: ${data['todos']}");
  List<dynamic> filteredData = data['todos'].where((item) => item["status"] == true).toList();
  return filteredData;
}

formatedFalseRichText(data) {
  //print("DATA DATA DATA:: ${data['todos']}");
  List<dynamic> filteredData = data['todos'].where((item) => item["status"] == false).toList();
  return filteredData;
}