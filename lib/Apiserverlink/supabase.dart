
//fetch projects from supabase

import 'dart:convert';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo/Apiserverlink/credentials.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:todo/variables/statecontroll.dart';
final supabase = Supabase.instance.client;



//fetch project

final ProjectDataFetchProvider = FutureProvider((ref) async{
 // await ref.refresh(funcCompProvider.future);
  final data = supabase.from('projects').select("*,todos(*)");
  //print('FROM DATA FETCH :: ${await data}');

  
 return data;
});
 

// ignore: non_constant_identifier_names
ProjectDataAdd(title,createdDate,{List? listoftodos})async{
  final todos =  jsonEncode(listoftodos);
  try {
  await supabase
    .from('projects')
    .insert({'title': title,'createdDate':createdDate});
    return 'success';
} on Exception catch (e) {
  return 'error';
}
}

//delete data
final deleteprojectProvider = FutureProvider.family<List<Map<String,dynamic>>,String>((ref,id) {
  
  return supabase.from('projects').delete().match({'idpr':id}).select();
});

//app todos

TodoDataAdd(todos,
createdDate,
description,
int releted_id,
bool status)async{
  
  try {
  await supabase
    .from('todos')
    .insert({'todos': todos,'createdDate':createdDate,
    'description':description,'Related_id':releted_id,
    'status':status});
    
    return 'success';
} on Exception catch (e) {
 // print(e);
  return 'error';
}
}
UpdateDataTodo(bool status,int id,updatedDate)async{
    try {
      await supabase
     .from('todos')
     .update({'status':status,'updatedDate':updatedDate})
     .eq('id',id);
      return'success';
    } on Exception catch (e) {
      return 'error';
    }
  }

//call a postgress function
 
//late int idpr;
final funcCompProvider = FutureProvider.family<dynamic,int>((ref,idpr) async{
  //final idpr = ref.watch(numberofCompletedProject);
  final data = await supabase
  .rpc('get_completed_task_count',params: { "project_id": "$idpr" });
  //print('POST FUNC  $data');
  return data;
});
//delete todos
final deleteTodoProvider = FutureProvider.family<List<Map<String,dynamic>>,String>((ref,id) async{
  final data = supabase.from('todos').delete().match({'id':id}).select();
  
  return data;
});