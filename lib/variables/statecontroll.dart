import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo/screens/rich.dart';

//state notifer 
final addProjectstate = StateProvider((ref) => false);
final projectStateindex = StateProvider((ref) => 0);
final numberofCompletedProject = StateProvider((ref) => 0);

final markdownProvider = StateNotifierProvider((ref) {
  return MarkdownProject();
});
