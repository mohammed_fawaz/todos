
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:todo/Apiserverlink/credentials.dart';
import 'package:todo/database/base.dart';
import 'package:todo/screens/login.dart';
import 'package:todo/screens/markdown.dart';
import 'package:todo/screens/project.dart';
import 'package:todo/variables/color_schemes.g.dart';


void main(List<String> args)async {
  await Supabase.initialize(
    url: url,
    anonKey: anonKey,
  );
  //ensure installed
  
  await Hive.initFlutter();
  Hive.registerAdapter(ProjectAdapter());
  
  runApp(ProviderScope(child: HomeRouter()));
}

class HomeRouter extends StatelessWidget {
  const HomeRouter({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
      theme: ThemeData(useMaterial3: true, colorScheme: lightColorScheme),
      darkTheme: ThemeData(useMaterial3: true, colorScheme: darkColorScheme),
      initialRoute: '/',
    routes: {
      '/':(context) => Login(),
      '/todos':(context) => RegisterProject(),
      '/myp':(context) => MarkDownPage()
      
    },);
  }
}