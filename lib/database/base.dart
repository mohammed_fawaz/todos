import 'package:hive/hive.dart';

part 'base.g.dart';
@HiveType(typeId: 0)
class Project extends HiveObject {
  @HiveField(0)
  final String title;

  @HiveField(1)
  final DateTime createdDate;

  @HiveField(2)
  final List<String> todos; // Store todo IDs instead of full Todos

  Project({
    required this.title,
    required this.createdDate,
    this.todos = const [],
  });
}



@HiveType(typeId: 1)
class Todos extends HiveObject {
  @HiveField(0)
  final String description;

  @HiveField(1)
  final bool status;

  @HiveField(2)
  final DateTime createdDate;

  @HiveField(3)
  final DateTime? updatedDate;

  Todos({
    required this.description,
    required this.status,
    required this.createdDate,
    this.updatedDate,
  });
}